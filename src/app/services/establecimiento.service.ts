import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EstablecimientoModel } from 'app/models/establecimiento.model';

@Injectable({
  providedIn: 'root'
})
export class EstablecimientoService {

  constructor( private http: HttpClient) { }

  listar(): EstablecimientoModel[]{
    return [
      {
        idEstablecimiento:1,
        ruc:"11111111111",
        razonSocial:"Clinica DentiMax",
        porcentajeGanancia: 30,
      },
      {
        idEstablecimiento:2,
        ruc:"11111111111",
        razonSocial:"Clinica DentiMax",
        porcentajeGanancia: 30,
      },

    ]

  }
}
