export class EstablecimientoModel {
    idEstablecimiento: number;
    ruc:String;
    razonSocial:String;
    porcentajeGanancia: number; 
}
