export class Proveedor {
    idProveedor: number;
    nombre:String;
    lugar:String;
    correo:String;
    telefono:String;
}
