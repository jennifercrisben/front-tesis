import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatSnackBar } from "@angular/material/snack-bar"; 
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { Router } from "@angular/router";
import { EstablecimientoService } from 'app/services/establecimiento.service';
import { EstablecimientoModel } from 'app/models/establecimiento.model';

@Component({
  selector: 'app-establecimiento',
  templateUrl: './establecimiento.component.html',
  styleUrls: ['./establecimiento.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None,
})

export class EstablecimientoComponent implements OnInit {

    public dataSource = new MatTableDataSource<any>();
    idEstablecimiento: number;
    visible: boolean;

    dialogRef: any;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator)
    set paginator(v: MatPaginator) {
        this.dataSource.paginator = v;
    }

    columnNames = [
        "idEstablecimiento",
        "ruc",
        "razonSocial",
        "porcentajeGanancia",
        "editar",
        "eliminar",
    ];

    constructor(
        private establecimientoService: EstablecimientoService,
        private router: Router,
        private _matDialog: MatDialog,
        private _matSnackBar: MatSnackBar
    ) {}


  ngOnInit(): void {
    //this.visible = false;
    this.dataSource.data = this.establecimientoService.listar();
    //this.dataSource.sort = this.sort;
    //this.visible = true;
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  };

  crear(): void {
    /*
    this.dialogRef = this._matDialog.open(BancoFormComponent, {
        panelClass: "banco-form-dialog",
        data: {
            action: "nuevo",
        },
    });
    this.dialogRef.afterClosed().subscribe((result) => {
        this.ngOnInit();
    });*/
  }
  
  editar(id: number): void {
      /*this.dialogRef = this._matDialog.open(BancoFormComponent, {
          panelClass: "banco-form-dialog",
          data: {
              action: "editar",
              id: id,
          },
      });
      this.dialogRef.afterClosed().subscribe((result) => {
          this.ngOnInit();
      });*/
  }

  eliminar(establecimiento: EstablecimientoModel): void {/*
      this.dialogRef = this._matDialog.open(MessageBoxComponent, {
          panelClass: "confirm-form-dialog",
          data: {
              titulo: "Eliminar banco: " + banco.nombre,
              subTitulo: "¿Está seguro de eliminar el banco?",
              botonAceptar: "Si",
              botonCancelar: "No",
          },
      });

      this.dialogRef.afterClosed().subscribe((result) => {
          if (localStorage.getItem("eliminar") === "S") {
              this.bancoService.eliminar(banco).subscribe(
                  (data) => {
                      this._matSnackBar.open("Se eliminó el banco", "", {
                          verticalPosition: "top",
                          horizontalPosition: "center",
                          duration: 5000,
                      });
                      this.ngOnInit();
                  },
                  (err) => {
                      this._matSnackBar.open(
                          "Error, banco no eliminado",
                          "",
                          {
                              verticalPosition: "top",
                              duration: 5000,
                          }
                      );
                  }
              );
          }
          localStorage.setItem("eliminar", "N");
      });*/
    } 

}
