import { FuseNavigation } from '@fuse/types';
//import {inventory_2} from '@angular/material/icon';

export const navigation: FuseNavigation[] = [
    {
        id: "agenda",
        title: "Agenda",
        type: "item",
        icon: "today",
        url: "/dentista/agenda",
    },
    {
        id: "pacientes",
        title: "Pacientes",
        type: "item",
        icon: "person_add",
        url: "/dentista/pacientes",
    },
    {
        id: "procedimiento",
        title: "Procedimientos",
        type: "item",
        icon: "healing",
        url: "/dentista/procedimiento",
    },
    {
        id: "inventario",
        title: "Inventario",
        type: "item",
        icon: "assignment",
        url: "/dentista/inventario",
    },
    {
        id: "caja",
        title: "Caja",
        type: "item",
        icon: "import_export",
        url: "/dentista/caja",
    },
    {
        id       : 'Configuracion',
        title    : 'Configuración',
        //translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            {
                id       : 'establecimiento',
                title    : 'Establecimientos',
                //translate: 'NAV.SAMPLE.TITLE',
                type     : 'item',
                icon     : "business",
                url      : '/dentista/configuracion/establecimiento',
                /*badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }*/
            },
            {
                id       : 'proveedor',
                title    : 'Proveedores',
                //translate: 'NAV.SAMPLE.TITLE',
                type     : 'item',
                icon     : 'people',
                url      : '/dentista/configuracion/proveedor',
            },
        ]
    },

];
